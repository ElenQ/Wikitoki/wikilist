import sqlite3
import json
import re
import csv
import sys
import os
from   functools import reduce
import operator

"""
This tool is the tool I used for the first Database load.
It doesn't work with the Excel file directly, it needs some manual changes, but
it's nice to have some examples and inspiration.
"""

source_folder = sys.argv[1]
target_db     = sys.argv[2]

def fileload( filename ):
    with open(filename) as f:
        reader = csv.DictReader(f)
        conts = tuple(reader)
    return conts

def mergecategory( conts, catname ):
    return tuple(map(lambda x: dict(x, CATEGORIA=catname), conts))

filenames = os.listdir(source_folder)
fullpaths = map(lambda x: os.path.join( source_folder, x), filenames)
catnames  = map(lambda x: x.split('.')[0], filenames)
catnames  = map(lambda x: x[0].upper()+x[1:], catnames)
read      = map(lambda x,y: mergecategory(fileload(x), y), fullpaths, catnames)
res       = reduce(operator.add, read)


valid_keys = ("TELÉFONO","CATEGORIA", "IDIOMA", "MAIL", "MEDIO", "DIRECCIÓN", "COMENTARIOS", "CONTACTO", "ÁMBITO")
filtered_res = map( lambda row: { k: row[k].strip() if k in row else "" for k in valid_keys }, res)


conn = sqlite3.connect(target_db)
cur = conn.cursor()
# Missing keys are not processed!
from collections import defaultdict
for i in filtered_res:
    params = defaultdict(lambda: None, i)

    if re.match("^\s?&", "".join(params.values())):
        continue

    cur.execute("INSERT INTO contacts (phone, category, email, address, notes, contact, organization, department ) VALUES (:TELÉFONO,:CATEGORIA,:MAIL, :DIRECCIÓN, :COMENTARIOS, :CONTACTO, :MEDIO, :DEPARTAMENTO)", params)
    if params["IDIOMA"] != None:
        last = cur.lastrowid
        idiomas = []
        if "EUSK" in params["IDIOMA"].upper():
            idiomas.append("eu")
        if "CASTELLANO" in params["IDIOMA"].upper():
            idiomas.append("es")
        if "ESPAÑOL" in params["IDIOMA"].upper():
            idiomas.append("es")
        if "INGL" in params["IDIOMA"].upper():
            idiomas.append("en")
        if "ENGL" in params["IDIOMA"].upper():
            idiomas.append("en")
        for lang in idiomas:
            cur.execute("INSERT INTO rel_contacts_languages (id_contact, id_language) VALUES (?,?)", (last, lang))
conn.commit()
conn.close()
