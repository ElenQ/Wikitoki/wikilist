(ns wikilist.layout
  (:require [selmer.parser :as parser]
            [selmer.filters :as filters]
            [clojure.data.csv :as csv]
            [ring.util.http-response :refer [content-type ok]]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]))


(parser/set-resource-path!  (clojure.java.io/resource "templates"))
(parser/add-tag! :csrf-field (fn [_ _] (anti-forgery-field)))

(defn render
  "renders the HTML template located relative to resources/templates"
  [template & [params]]
  (content-type
    (ok
      (parser/render-file
        template
        (assoc params
          :page template
          :csrf-token *anti-forgery-token*)))
    "text/html; charset=utf-8"))

(defn error-page
  "error-details should be a map containing the following keys:
   :status - error status
   :title - error title (optional)
   :message - detailed error message (optional)

   returns a response map with the error page as the body
   and the status specified by the status key"
  [error-details]
  {:status  (:status error-details)
   :headers {"Content-Type" "text/html; charset=utf-8"}
   :body    (parser/render-file "error.html" error-details)})

(defn render-vcard
  [params]
  (content-type
    (ok (parser/render-file "vcard.vcf" params))
    "text/vcard; charset=utf-8"))



; CSV writers
(defn map->vec-vals
  [fields data]
  (vec (map #(%1 data) fields)))

(defn map->csv
  [data]
  (let [fields (sort (keys (first data)))
        header (map name fields)
        vals   (vec (map #(map->vec-vals fields %) data))]
    (with-out-str (csv/write-csv *out* (concat [header] vals)))))

(defn render-csv
  [params]
  (content-type
    (ok (map->csv params))
    "text/csv; charset=utf-8"))

