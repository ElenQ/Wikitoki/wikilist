(ns wikilist.routes.contacts
  (:require [wikilist.layout :as layout]
            [wikilist.db.core :as db]
            [wikilist.db.contacts :refer :all]
            [wikilist.utils :refer :all]
            [wikilist.validation.contacts :refer :all]
            [compojure.core :refer [defroutes context GET POST]]
            [ring.util.http-response :as response]
            [clojure.walk :as walk]
            [clojure.string :refer [join]]))

(defn- form-to-id-list
  "Converts the {:__anti-forgery-token $token :$id true/false ...}-like form
  structure to an id list"
  [fparams]
  (->> (dissoc fparams :__anti-forgery-token)
       walk/keywordize-keys
       keys
       (map name)
       (assoc {} :ids)))

(defn- render-contacts!
  [iden querystring [error filterby success]]
  (layout/render "contacts.html" {:contacts      (get-contacts-by! filterby)
                                  :identity      iden
                                  :filterby      filterby
                                  :querystring   querystring
                                  :error         error
                                  :success       success
                                  :categories    (db/get-used-categories!)
                                  :orgs          (db/get-used-orgs!)
                                  :languages     (db/get-used-languages!)}))

(defn- render-edit-contact!
  [iden [error contact success]]
  (layout/render "contact_edit.html" {:contact       contact
                                      :error         error
                                      :success       success
                                      :categories    (db/get-used-categories!)
                                      :languages     (db/get-languages!)
                                      :orgs          (db/get-used-orgs!)
                                      :identity      iden}))
(defn- render-view-contact!
  [iden [error contact success]]
  (layout/render "contact.html" {:contact       contact
                                 :error         error
                                 :categories    (db/get-used-categories!)
                                 :languages     (db/get-languages!)
                                 :orgs          (db/get-used-orgs!)
                                 :identity      iden}))

(def contacts-read
  (context
    "/contacts" []
    (GET "/" [& filterby]
         (fn [req]
           (render-contacts! (:identity req)
                             (:query-string req)
                             [nil filterby])))

    (POST "/export/mailto" [:as {qparams :query-params fparams :form-params}]
          (fn [req]
            ; Validate ids?
            (->> fparams
                 form-to-id-list
                 get-contacts!
                 (map #(:email %))
                 (filter #(not= % ""))
                 (join "&bcc=")
                 (str "mailto:?bcc=")
                 (assoc {} :mailto)
                 (layout/render "mailto.html"))))

    (POST "/export/vcard" [:as {qparams :query-params fparams :form-params}]
          (fn [req]
            ; Validate ids?
            (->> fparams
                 form-to-id-list
                 get-contacts!
                 (assoc {} :contacts)
                 layout/render-vcard)))

    (POST "/export/csv" [:as {qparams :query-params fparams :form-params}]
          (fn [req]
            ; Validate ids?
            (->> fparams
                 form-to-id-list
                 get-contacts!
                 (map #(dissoc % :languages :id))
                 layout/render-csv)))

    (POST "/export/print" [:as {qparams :query-params fparams :form-params}]
          (fn [req]
            ; Validate ids?
            (->> fparams
                 form-to-id-list
                 get-contacts!
                 (assoc {} :contacts)
                 (layout/render "printable.html"))))))


(def contacts-edit
  (context "/contacts" []
    ; New contact form
    (GET "/new" [& params]
         (fn [req]
           (render-edit-contact! (:identity req)
                                 [nil
                                  ; Sets the values to the
                                  ; filter, to simplify user
                                  ; creation
                                  ; TODO extend to more fields
                                  {:category (:category params)}])))

    ; Edit contact form
    (GET "/:id" [id & params]
         (fn [req]
           (render-edit-contact! (:identity req)
                                 (get-contact! {:id id}))))

    ; Delete contacts
    (POST "/" [:as {qparams :query-params fparams :form-params}]
          (fn [req]
            (let [filterby (walk/keywordize-keys qparams)
                  delete   (form-to-id-list fparams)]
              (->> delete
                   validate-ids-selection-contacts
                   (bind-error delete-contacts!)
                   (bind-error (partial success-message
                                        "Contacts deleted successfully."))
                   ((fn [[err data succ]] [err filterby succ]))
                   (render-contacts! (:identity req)
                                     (:query-string req))))))


    ; Edit contacts
    (POST "/new" [& params]
          (fn [req]
            (->> (dissoc params :__anti-forgery-token)
                 validate-contact
                 (bind-error create-contact-full!)
                 (bind-error (fn [data] [nil {}])) ; Clean Form, contact is stored
                 (bind-error (partial success-message
                                      (str "Contact created successfully. "
                                           "Go back or create a new contact.")))
                 (render-edit-contact! (:identity req)))))
    (POST "/:id" [& params]
          (fn [req]
            (->> (dissoc params :__anti-forgery-token)
                 validate-contact
                 (bind-error update-contact-full!)
                 (bind-error (partial success-message
                                      "Contact updated successfully."))
                 (render-edit-contact! (:identity req)))))))
