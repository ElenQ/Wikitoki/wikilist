(ns wikilist.routes.login
  (:require [wikilist.layout :as layout]
            [wikilist.db.core :as db]
            [compojure.core :refer [defroutes GET POST]]
            [ring.util.http-response :as response]
            [clojure.java.io :as io]
            [buddy.hashers :as hashers]))


(defn check-identity
  "Checks if the user exists and if the password is correct.
  Returns the id of the user and the role if is correct, nil if it isn't"
  [email pass]
  (when-let [user-data (db/get-user-by-email! {:email email})]
    (when (hashers/check pass (:pass user-data))
      (select-keys user-data [:id :role]))))

(defroutes login-pages
  (GET  "/login"  [] (layout/render "login.html"))
  (GET  "/login-required" []
       (layout/render "login.html" {:error {:message "Login required"
                                            :level   "error"}})))

(defroutes login-methods
  (POST "/login"  [email password]
        (fn [req]
          (if-let [user (check-identity email password)]
            (assoc (response/see-other "/")
                   :session (assoc (:session req) :identity user))
            (layout/render "login.html" {:error {:message "Login failed"
                                                 :level   "error"}})))))
(defroutes logout-methods
  (GET "/logout" []
       (fn [req]
         (assoc (response/see-other "/login")
                :session (dissoc (:session req) :identity)))))


; Rich comments ;)
(comment
  (do (hashers/derive "secretpassword"); bcrypt+sha512$...
      (hashers/check "secretpassword" "bcrypt+sha512$fd0aa4a3146406c8c4b36abb34e4b13b$12$2c0676bad14d0a453406ba8824f6e955f9546d999aca1e7a"); true
      (hashers/check "secretpassword" "bcrypt+sha512$fd0aa4a3146406c8c4b36abb34e4b13b$12$2c0676bad14d0a453406ba8824f6e955f9546d999aca1e7b"); false
      (check-identity "ekaitz@elenq.tech" "pass")))
(comment
  (db/create-user! {:first_name "Ekaitz"
                    :last_name  "Zarraga"
                    :email      "ekaitz@elenq.tech"
                    :role       "admin"
                    :pass       (hashers/derive "pass") }))

