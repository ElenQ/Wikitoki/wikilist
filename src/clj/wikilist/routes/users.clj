(ns wikilist.routes.users
  (:require [wikilist.layout :as layout]
            [wikilist.db.core :as db]
            [wikilist.db.users :refer :all]
            [wikilist.utils :refer :all]
            [compojure.core :refer [defroutes context GET POST]]
            [ring.util.http-response :as response]
            [buddy.hashers :as hashers]
            [wikilist.validation.users :refer :all]))

(defn- derive-pass
  [data]
  [nil (update data :pass hashers/derive)])

(defn- prepare-pass
  "If there's a pass in the form, it derives it to insert it in the DB. If
  the pass is empty it removes it because the user didn't enter it."
  [data]
  [nil
   (if (not= "" (:pass data))
     (update data :pass hashers/derive)
     (dissoc data :pass))])

(defn- remove-pass
  [data]
  [nil (dissoc data :pass :pass_confirm)])




(defn- render-user!
  [iden [error user success]]
  (layout/render "user.html" {:error error
                              :user user
                              :success success
                              :roles (db/get-valid-roles!)
                              :identity iden}))
(defn- render-users!
  [iden [error role-filter success]]
  (layout/render "users.html" {:success success
                               :error error
                               :users (get-users-by-role! role-filter)
                               :roles (db/get-valid-roles!)
                               :identity iden
                               :filterby role-filter}))


(def users-read
  (context "/users" []
    (GET "/" [role]
         (fn [req]
           (render-users! (:identity req)
                          [nil role])))

    (GET "/new" [role]
         (fn [req]
           (render-user! (:identity req) [nil {:new true
                                               :role role}])))

    (GET "/:id" [id]
         (fn [req]
           (if-let [user (db/get-user! {:id id})]
             (render-user! (:identity req) [nil user])
             (response/see-other "/users/")))))) ; If it doesn't exist go out

(def users-edit
  (context "/users" [req]

    (POST  "/new" [& params]
          (fn [req]
            (->> (dissoc params :__anti-forgery-token)
                 validate-new-user
                 (bind-error derive-pass)
                 (bind-error create-user!)
                 (bind-error (fn [data] [nil {}])) ; Clean Form, user is stored
                 (bind-error (partial success-message
                                      (str "User stored successfully. "
                                           "Go back or create a new user.")))
                 (render-user! (:identity req)))))

    (POST  "/" [role & params]
          (fn [req]
            (->> (dissoc params :__anti-forgery-token)
                 keys
                 (map name)
                 (assoc {} :ids)
                 validate-delete-users
                 (bind-error delete-users!)
                 (bind-error (partial success-message
                                      "Users deleted successfully."))
                 ((fn [[err data succ]] [err role succ]))
                 (render-users! (:identity req)))))

    (POST  "/:id" [& params]
          (fn [req]
            (->> (dissoc params :__anti-forgery-token)
                 validate-update-user!
                 (bind-error prepare-pass)
                 (bind-error (fn [data]
                               [nil
                                (merge (db/get-user! (select-keys data [:id]))
                                       data)]))
                 (bind-error update-user!)
                 (bind-error remove-pass)
                 (bind-error (partial success-message
                                      "User updated successfully"))
                 (render-user! (:identity req)))))))
