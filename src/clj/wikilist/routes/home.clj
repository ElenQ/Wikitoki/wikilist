(ns wikilist.routes.home
  (:require [wikilist.layout :as layout]
            [compojure.core :refer [defroutes GET]]
            [ring.util.http-response :as response]))


(defroutes home-routes
  (GET "/" [] (response/see-other "/contacts")))
