(ns wikilist.handler
  (:require
            [wikilist.layout :refer [error-page]]
            [compojure.core :refer [routes wrap-routes]]
            [compojure.route :as route]
            [wikilist.env :refer [defaults]]
            [mount.core :as mount]
            [wikilist.middleware :as middleware]

            ; Load the routes
            [wikilist.routes.home :refer :all]
            [wikilist.routes.contacts :refer :all]
            [wikilist.routes.users :refer :all]
            [wikilist.routes.login :refer :all]))

(mount/defstate init-app
  :start ((or (:init defaults) identity))
  :stop  ((or (:stop defaults) identity)))

(mount/defstate app
  :start
  (middleware/wrap-base
    (routes
      ; Home redirect to Contacts
      (-> #'home-routes
          (wrap-routes middleware/wrap-csrf)
          (wrap-routes middleware/wrap-at-least-reader)
          (wrap-routes middleware/wrap-restricted))

      ; Login / Logout
      (-> #'login-pages
          (wrap-routes middleware/wrap-csrf))
      (-> #'login-methods
          (wrap-routes middleware/wrap-csrf))
      (-> #'logout-methods
          (wrap-routes middleware/wrap-restricted))

      ; Users
      (-> #'users-read
          (wrap-routes middleware/wrap-csrf)
          (wrap-routes middleware/wrap-admin)
          (wrap-routes middleware/wrap-restricted))
      (-> #'users-edit
          (wrap-routes middleware/wrap-csrf)
          (wrap-routes middleware/wrap-admin)
          (wrap-routes middleware/wrap-restricted))

      ; Contacts
      (-> #'contacts-read
          (wrap-routes middleware/wrap-csrf)
          (wrap-routes middleware/wrap-at-least-reader)
          (wrap-routes middleware/wrap-restricted))
      (-> #'contacts-edit
          (wrap-routes middleware/wrap-csrf)
          (wrap-routes middleware/wrap-at-least-editor)
          (wrap-routes middleware/wrap-restricted))

      (route/not-found
        (:body
          (error-page {:status 404
                       :title "Page not found"}))))))
