(ns wikilist.utils)

(defn bind-error [f [err value]]
  (if (nil? err)
    (f value)
    [err value]))

(defn success-message
  "Inserts a success-message in the data flow, that way a success message can
  be rendered in the template without affecting the rest of the data flow."
  [message data]
  [nil data message])
