(ns wikilist.middleware
  (:require [wikilist.env :refer [defaults]]
            [clojure.tools.logging :as log]
            [wikilist.layout :refer [error-page]]
            [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
            [ring.middleware.webjars :refer [wrap-webjars]]
            [ring.util.http-response :refer [found]]
            [muuntaja.core :as muuntaja]
            [muuntaja.format.json :refer [json-format]]
            [muuntaja.middleware :refer [wrap-format wrap-params]]
            [wikilist.config :refer [env]]
            [immutant.web.middleware :refer [wrap-session]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [buddy.auth.accessrules :refer [restrict]]
            [buddy.auth :refer [authenticated?]]
            [buddy.auth.backends.session :refer [session-backend]])
  (:import  [javax.servlet ServletContext]))

(defn wrap-internal-error [handler]
  (fn [req]
    (try
      (handler req)
      (catch Throwable t
        (log/error t (.getMessage t))
        (error-page {:status 500
                     :title "Something very bad has happened!"
                     :message "We've dispatched a team of highly trained gnomes to take care of the problem."})))))

(defn wrap-csrf [handler]
  (wrap-anti-forgery
    handler
    {:error-response
     (error-page
       {:status 403
        :title "Invalid anti-forgery token"})}))

; JSON Format
(def restful-format-options
  (muuntaja/create
    (muuntaja/select-formats
      muuntaja/default-options
      ["application/json"])))
(defn wrap-formats [handler]
  (let [wrapped (-> handler wrap-params (wrap-format restful-format-options))]
    (fn [request]
      (wrapped request))))

; Restriction per role
; Helpers
(defn errorf
  ([]
   (fn [request response]
     (error-page
       {:status 403
        :title (str "Access to " (:uri request) " is not authorized")})))
  ([message]
   (fn [request response]
     (error-page
       {:status 403
        :title (str "Access to " (:uri request) " is not authorized: " message)}))))
(defn admin?
  [request]
  (= "admin" (:role (:identity request))))
(defn editor?
  [request]
  (= "editor" (:role (:identity request))))
(defn reader?
  [request]
  (= "reader" (:role (:identity request))))

; Role requirements, they fail with a 403
(defn wrap-admin
  [handler]
  (restrict handler
            {:handler  admin?
             :on-error (errorf "Must be admin")}))

(defn wrap-at-least-editor
  [handler]
  (restrict handler
            {:handler  (fn [r] (or (editor? r) (admin? r)))
             :on-error (errorf "Must be at least editor")}))

(defn wrap-at-least-reader
  [handler]
  (restrict handler
            {:handler  (fn [r] (or (reader? r) (editor? r) (admin? r)))
             :on-error (errorf "Must be at least reader")}))

; Login required, redirect to login-required if failed
(defn wrap-restricted [handler]
  (restrict handler {:handler authenticated?
                     :on-error (fn [request metadata] (found "/login-required"))}))


; General Auth middleware
(defn wrap-auth [handler]
  (let [backend (session-backend)]
    (-> handler
        (wrap-authentication backend)
        (wrap-authorization  backend))))

(defn wrap-base [handler]
  (-> ((:middleware defaults) handler)
      wrap-auth
      wrap-webjars
      (wrap-session {:cookie-attrs {:http-only true}})
      (wrap-defaults
        (-> site-defaults
            (assoc-in [:security :anti-forgery] false)
            (dissoc :session)))
      wrap-internal-error))
