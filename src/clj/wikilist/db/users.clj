(ns wikilist.db.users
  (:require
    [wikilist.db.core :as db]
    [clojure.tools.logging :as log]))

(defn- edit-user!
  "Edits user in database. Input data expected in [previous-error data] pairs.
  Returns [error data] pair, if input has an error it acts like identity. Used
  to create update and create functions."
  [func user]
  (let [error {:db "Database error: unable to write in database, check if user is already registered"}]
    (try
      (if (= 1 (func user))
        [nil user]
        [error user])
      (catch Exception ex
        (log/error ex "Exception in user edition")
        [error user]))))
(def create-user! (partial edit-user! db/create-user!))
(def update-user! (partial edit-user! db/update-user!))

(defn delete-users!
  "Deletes users that match the IDs sent as input"
  [ids]
  (try
    (db/delete-users! ids)
    [nil nil]
    (catch Exception ex
      (log/error ex "Exception in user deletion")
      [{:db "Database error: unable to delete users"} nil])))

(defn get-users-by-role!
  "Retrieves users by role, doesn't check errors"
  [role]
  (->> (db/get-all-users!)
       (map #(dissoc % :pass))
       (filter #(or (nil? role)
                    (= role (:role %))))))
