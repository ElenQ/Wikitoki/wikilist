(ns wikilist.db.contacts
  (:require [wikilist.db.core :as db]
            [clojure.tools.logging :as log]))

(defn get-contact!
  "Get a contact by its id"
  [data]
  (let [error {:db "Database error: unable to get data from database"}
        id    (:id data)]
    (try
      [nil (assoc (db/get-contact! {:id id})
                  :languages
                  (map #(:id %) (db/get-attached-languages! {:id_contact id})))]
      (catch Exception ex
        (log/error ex "Exception in contact selection")
        [error nil]))))

(defn delete-contacts!
  "Deletes contacts that match the IDs sent as input"
  [ids]
  (try
    (db/delete-contacts! ids)
    [nil nil]
    (catch Exception ex
      (log/error ex "Exception in contact deletion")
      [{:db "Database error: unable to delete contacts"} nil])))

(defn create-contact-full!
  "Creates a contact and associates the languages"
  [contact]
  (try
    (let [done       (db/create-contact! contact)
          id         (:id (db/last-inserted!))
          langs      (flatten [(:languages contact)])] ; Convert to list of langs
                                                       ; If it's one, it comes
                                                       ; as an string.
      (doseq [lang langs]
        (db/attach-language! {:id_contact id :id_language lang}))
      [nil (assoc contact :languages langs :id id)])
    (catch Exception ex
      (log/error ex "Exception in contact creation")
      [{:db "Database error: unable to write in database"} contact])))

(defn update-contact-full!
  "Updates a contact and the associated languages"
  [contact]
  (try
    (let [done        (db/update-contact! contact)
          id          (:id contact)
          langs       (flatten [(:languages contact)]) ; Convert to list of langs
                                                       ; If it's one, it comes
                                                       ; as an string.
          att-langs   (map #(:id %) (db/get-attached-languages! {:id_contact id}))
          not-in-list (fn [el coll] (some #(not= el %) coll))]
      (doseq [detach-langs   (filter #(not-in-list % langs) att-langs)]
        (db/detach-language! {:id_contact id :id_language detach-langs}) )
      (doseq [attach-langs   (filter #(not-in-list % att-langs) langs)]
        (db/attach-language! {:id_contact id :id_language attach-langs}) )
      [nil (assoc contact :languages langs)])
    (catch Exception ex
      (log/error ex "Exception in contact update")
      [{:db "Database error: unable to write in database"} contact])))


; Without error binding
; TODO CHECK HOW TO MAKE THE LANGUAGE JOINS IN SQL AND NOT HERE
(defn get-contacts-by!
  "Get contacts by the selected filter"
  [filterby]
  (try
    (->> (db/get-contacts-by! filterby)
         (map (fn [c] (assoc c :languages
                             (map #(:id %)
                                  (db/get-attached-languages! {:id_contact (:id c)}))))))
    (catch Exception ex
      (log/error ex "Exception in get-contacts-by!"))))

(defn get-contacts!
  "Get contacts by a list of ids"
  [ids]
  (try
    (->> (db/get-contacts! ids)
         (map (fn [c] (assoc c :languages
                             (map #(:id %)
                                  (db/get-attached-languages! {:id_contact (:id c)}))))))
    (catch Exception ex
      (log/error ex "Exception in get-contacts!"))))
