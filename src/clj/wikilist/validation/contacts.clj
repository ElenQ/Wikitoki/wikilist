(ns wikilist.validation.contacts
  (:require [struct.core :as st]
            [wikilist.db.core :as db]))

(defn validate-contact
  "Validates if input contact data is correct for registration"
  [contact]
  (let
    [contact-schema {
                  :category          [[st/required :message "Category required"]
                                      [st/string   :message "Invalid category"]]
                  :organization      [[st/required :message "Organization required"]
                                      [st/string   :message "Invalid organization"]]
                  :department        [[st/string   :message "Invalid department"]]
                  :location          [[st/string   :message "Invalid location"]]
                  :email             [[st/email    :message "Invalid email"]]
                  :phone             [[st/string   :message "Invalid phone"]]
                  :address           [[st/string   :message "Invalid address"]]
                  :website           [[st/string   :message "Invalid website"]]
                  :notes             [[st/string   :message "Invalid notes"]]
                  :contact           [[st/string   :message "Invalid contact name"]]}]
    (if (= "" (:email contact))
      (st/validate contact (dissoc contact-schema :email)) ; NOTE:
                                                           ; hack for optional
                                                           ; email, email
                                                           ; validator makes it
                                                           ; mandatory
      (st/validate contact contact-schema))))

(defn validate-ids-selection-contacts
  [form]
  (let [schema {:ids {:message "At least one user needed"
                      :validate (fn [l] (< 0 (count l)))}}]
    (st/validate form schema)))
