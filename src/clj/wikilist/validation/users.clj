(ns wikilist.validation.users
  (:require [struct.core :as st]
            [wikilist.db.core :as db]))

(defn validate-new-user
  "Validates if input user data is correct for registration"
  [user]
  (let
    [user-schema {:email             [[st/required :message "Email required"]
                                      [st/email    :message "Invalid email"]]
                  :first_name        [[st/required :message "First name required"]
                                      [st/string   :message "Invalid first name"]]
                  :last_name         [[st/string   :message "Invalid last name"]]
                  :pass              [[st/required :message "Password required"]
                                      [st/string   :message "Invalid password"]
                                      [st/min-count 7 :message "Password needs at least 7 characters"]]
                  :pass_confirm      [[st/required :message "Password confirmation required"]
                                      [st/string   :message "Invalid password confirmation"]
                                      [struct.core/identical-to :pass :message "Passwords do not match"]]
                  :role              [[st/required :message "Role required"]]
                  }]
    (st/validate user user-schema)))

(defn validate-update-user!
  "Validates if input user data is correct for update"
  [user]
  (let
    [user-schema {:id                [[st/required :message "Invalid request"]
                                      {:message "User doesn't exist"
                                       :validate (fn [id] (not (nil? (db/get-user! {:id id}))))}]
                  :email             [[st/required :message "Email required"]
                                      [st/email    :message "Invalid email"]]
                  :first_name        [[st/required :message "First name required"]
                                      [st/string   :message "Invalid first name"]]
                  :last_name         [[st/string   :message "Invalid last name"]]
                  :role              [[st/required :message "Role required"]]}
     with-pass   (merge user-schema
                        {:pass         [[st/required :message "Password required"]
                                        [st/string   :message "Invalid password"]
                                        [st/min-count 7 :message "Password needs at least 7 characters"]]
                         :pass_confirm [[st/required :message  "Password confirmation required"]
                                        [st/string   :message "Invalid password confirmation"]
                                        [struct.core/identical-to :pass :message "Passwords do not match"]]})]
   (if (as-> user u
         (select-keys u [:pass :pass_confirm])
         (vals u)
         (some #(not= % "") u))
     (st/validate user with-pass)
     (st/validate user user-schema))))

(defn validate-delete-users
  [form]
  ; TODO TEST THIS WELL
  (let [schema {:ids {:message "At least one user needed"
                      :validate (fn [l] (< 0 (count l)))}}]
    (st/validate form schema)))
