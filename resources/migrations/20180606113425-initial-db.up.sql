CREATE TABLE roles
(
    id         TEXT PRIMARY KEY
);
--;;
-- TODO check to do it with validation
INSERT INTO roles (id) VALUES ("admin"),
                              ("editor"),
                              ("reader");
--;;
CREATE TABLE users
(
    id         INTEGER PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name  TEXT NOT NULL,
    email      TEXT NOT NULL UNIQUE,
    role       TEXT NOT NULL,
    pass       TEXT NOT NULL,

    FOREIGN KEY(role) REFERENCES roles(id)
);
--;;
CREATE TABLE contacts
(
    id           INTEGER PRIMARY KEY,
    category     TEXT,
    organization TEXT,
    department   TEXT,
    location     TEXT,
    email        TEXT,
    phone        TEXT,
    address      TEXT,
    website      TEXT,
    notes        TEXT,
    contact      TEXT
);
--;;
CREATE TABLE rel_contacts_languages
(
    id_contact    INTEGER,
    id_language   TEXT,

    PRIMARY KEY (id_contact, id_language),
    FOREIGN KEY(id_contact) REFERENCES contacts(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    FOREIGN KEY(id_language) REFERENCES languages(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);
--;;
CREATE TABLE languages (
    id TEXT PRIMARY KEY,
    language TEXT
);
