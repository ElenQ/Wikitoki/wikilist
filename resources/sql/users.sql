-- USER QUERIES

-- :name get-valid-roles! :? :*
-- :doc retrieves the list of valid roles
SELECT id FROM roles



-- :name get-all-users! :? :*
-- :doc retrieves all the users
SELECT * FROM users

-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(first_name, last_name, email, role, pass)
VALUES (:first_name, :last_name, :email, :role, :pass)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET first_name = :first_name, last_name = :last_name, email = :email, role = :role, pass = :pass
WHERE id = :id

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id

-- :name delete-users! :! :n
-- :doc deletes users in list
DELETE FROM users
WHERE id IN (:v*:ids)

-- :name get-user! :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name get-user-by-email! :? :1
-- :doc retrieves a user record given the email
SELECT * FROM users
WHERE email = :email
