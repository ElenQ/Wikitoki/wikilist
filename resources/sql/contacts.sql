-- :name last-inserted! :? :n
-- :doc get last inserted id
SELECT last_insert_rowid() as id

-- :name create-contact! :! :n
-- :doc creates a new contact record
INSERT INTO contacts
(category, location, organization, department, contact, email, phone, address, website, notes)
VALUES (:category, :location, :organization, :department, :contact, :email, :phone, :address, :website, :notes)

-- :name update-contact! :! :n
-- :doc updates an existing contact record
UPDATE contacts
SET category = :category,
    location = :location,
    contact = :contact,
    organization = :organization,
    department = :department,
    email = :email,
    phone = :phone,
    address = :address,
    website = :website,
    notes = :notes
WHERE id = :id

-- :name delete-contact! :! :n
-- :doc deletes a registered contact
DELETE FROM contacts
WHERE id = :id

-- :name delete-contacts! :! :n
-- :doc deletes contacts in list
DELETE FROM contacts
WHERE id IN (:v*:ids)

-- :name get-contact! :? :1
-- :doc retrieves a contact record given the id, no joins made
SELECT * FROM contacts WHERE id = :id

-- :name get-contacts! :? :*
-- :doc retrieves contacts by id list
SELECT * FROM contacts
WHERE id IN (:v*:ids)

-- :name get-all-contacts! :? :*
-- :doc retrieves all the contacts on the DB
SELECT * FROM contacts ORDER BY organization

-- :name get-contacts-by-category! :? :*
-- :doc retrieves all the contacts with the category
SELECT * FROM contacts WHERE category = :category ORDER BY organization

-- :name get-contacts-by-language! :? :*
-- :doc retrieves all the contacts with the language
SELECT c.*, l.* FROM rel_contacts_languages r
    JOIN contacts c ON r.id_contact = c.id
    JOIN languages l ON l.id = r.id_language
WHERE l.language = :language ORDER BY c.organization




-- :name get-used-categories! :? :*
-- :doc get all distinct categories
SELECT DISTINCT(category) AS name FROM contacts ORDER BY category

-- :name get-used-orgs! :? :*
-- :doc get all distinct organizations
SELECT DISTINCT(organization) AS name FROM contacts ORDER BY organization




-- :name attach-language! :! :n
-- :doc inserts a language associated to a contact
INSERT INTO rel_contacts_languages (id_contact, id_language)
VALUES (:id_contact, :id_language)

-- :name detach-language! :! :n
-- :doc removes a language associated to a contact
DELETE FROM rel_contacts_languages
WHERE id_contact = :id_contact AND id_language = :id_language

-- :name get-attached-languages! :? :*
-- :doc retrieves all the languages attached to a contact
SELECT l.id FROM languages l JOIN rel_contacts_languages r
ON r.id_language = l.id
WHERE r.id_contact = :id_contact
--
-- :name get-used-languages! :? :*
-- :doc get all distinct languages in use
SELECT l.* FROM languages l JOIN rel_contacts_languages r
ON r.id_language = l.id GROUP BY l.id ORDER BY l.language


-- :name get-languages! :? :*
-- :doc get all languages
SELECT * FROM languages ORDER BY language


-- :name get-contacts-by! :? :*
-- :doc get with filter
SELECT * FROM contacts c
--~ (when (:language params) "LEFT JOIN rel_contacts_languages r ON r.id_contact = c.id")
WHERE 1
--~ (when (:category params) "AND category = :category")
--~ (when (:organization params) "AND organization = :organization")
--~ (when (:language params) "AND id_language = :language")
--~ (when (:contact params) "AND instr( lower(contact), lower(:contact) )")
ORDER BY c.organization
