# Wikilist

Wikilist is a contact database made for simplified search and storage of
contacts.

It's design is based on simplicity, low resource usage and stability. It's easy
to run and maintain.

It makes use of [Luminus](http://www.luminusweb.net/) a Clojure web framework
with no special configuration so their tutorials still apply.

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein run

## Running in production

Compilation to uberjar makes the application easy to deploy in any server,
compile it with:

    lein uberjar

And run it with:

    java -jar wikilist.jar

This web application uses SQLite for the database backend but it delegates the
foreign key support to it. Make sure to run it with `?foreign_keys=on;` in the
database URI of the jdbc connector.

Example for the `dev` environment:

``` clojure
{ :database-url "jdbc:sqlite:wikilist_dev.db?foreign_keys=on;" }
```

You can use environment variables to control the database connection too. More
info here:
[http://www.luminusweb.net/docs/environment.html](http://www.luminusweb.net/docs/environment.html)


## License

Copyright © 2018 Ekaitz Zárraga \<ekaitz@elenq.tech\>

This software is released under the terms of the Affero GPL License 3.0
version.

A copy of the license is distributed with the software, if it's not you can
read the license [here](https://www.gnu.org/licenses/agpl-3.0.en.html)
