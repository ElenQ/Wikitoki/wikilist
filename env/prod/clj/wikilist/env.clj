(ns wikilist.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[wikilist started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[wikilist has shut down successfully]=-"))
   :middleware identity})
