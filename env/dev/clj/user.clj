(ns user
  (:require [wikilist.config :refer [env]]
            [clojure.spec.alpha :as s]
            [expound.alpha :as expound]
            [mount.core :as mount]
            [wikilist.core :refer [start-app]]
            [wikilist.db.core]
            [conman.core :as conman]
            [luminus-migrations.core :as migrations]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(defn start []
  (mount/start-without #'wikilist.core/repl-server))

(defn stop []
  (mount/stop-except #'wikilist.core/repl-server))

(defn restart []
  (stop)
  (start))

(defn restart-db []
  (mount/stop #'wikilist.db.core/*db*)
  (mount/start #'wikilist.db.core/*db*)
  (binding [*ns* 'wikilist.db.core]
    (conman/bind-connection wikilist.db.core/*db* "sql/users.sql"
                                                  "sql/contacts.sql")))

(defn reset-db []
  (migrations/migrate ["reset"] (select-keys env [:database-url])))

(defn migrate []
  (migrations/migrate ["migrate"] (select-keys env [:database-url])))

(defn rollback []
  (migrations/migrate ["rollback"] (select-keys env [:database-url])))

(defn create-migration [name]
  (migrations/create name (select-keys env [:database-url])))


