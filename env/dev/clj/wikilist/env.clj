(ns wikilist.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [wikilist.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[wikilist started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[wikilist has shut down successfully]=-"))
   :middleware wrap-dev})
