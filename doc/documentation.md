# Wikilist development documentation

## Intro

This app is built using [Luminus Framework](http://www.luminusweb.net/).
Luminus' documentation is quite straightforward and this app follows it
precisely. That's why this documentation only covers special cases or
application specific things, all the rest is supposed to be easy to understand
following Luminus' docs.

## Application architecture

This app consists in various parts:

- SQLite based storage
- Clojure powered business logic
- Server side template based rendering engine
- Some JavaScript powered magic on client side

The arrangement of the files in the project follow Luminus' architecture:

- `src` for the clojure code
- `resources` for templates, migrations, SQL code and static resources.
- `doc` for this documentation you are reading at the moment

### Database

The app makes use of SQLite as database but this is easy to change to
PostgreSQL, MySQL or H2. [Luminus' documentation explain how to make the
change](http://www.luminusweb.net/docs/database.html).

All the code to deal with the database is located in `wikilist.db` namespace
and the `resources/sql` files. That's why HugSQL library uses SQL code directly
to access the database, and it automagically converts the SQL to Clojure
functions.

The `resources/migrations` folder contains the database migrations.

### Templates

All templates are contained in `resources/templates` folder and they are
rendered with the code at `wikilist.layout` namespace.

Semantic-UI was chose as frontend framework because it is full of interesting
features for forms. Forms are the 90% of this app, so it's important to handle
them easily.

Templates are quite big and have tons of logic. The idea was to keep all the
logic at server side. There are some client side scripts though: most of them
handle some Semantic-UI related things in forms.

### Validation

Validation is done at the `wikilist.validation` namespace using the `struct`
library.

### Routing

All the routes are contained in the `wikilist.routes` namespace and configured
in the `wikilist.handler` namespace using the middleware functions of
`wikilist.middleware`.

Each route pack has routes grouped in different contexts which have different
permission levels depending on the action they perform.

## Special details

### Contacts table form

The `contacts.html` template is quite complex, it supports searches and many
things more. In order to achieve that there are some unexpected details with
the form.

Most of the buttons access the main table-like form in different ways so keep
an eye on it and be careful if you want to edit it.

The filter form is also tricky. It's hidden but usable so it can be sent by the
filter button in order to activate its visualization.

### Error handling

`wikilist.utils` namespace contains the error handling mechanism. It avoids
using try/catch-like structures and it makes use of `bind-error` function. This
function supposes all functions involved return a tuple with two values:

- Error code (`nil` if error didn't happen)
- Value

That way, using a threading macro we can avoid code execution since the point
where an error happened and we are still able to render the template with some
useful information (like form validation). This is the standard way to do it:

``` clojure
(POST "/new" [& params]
      (fn [req]
        (->> (dissoc params :__anti-forgery-token)
             validate-contact
             (bind-error create-contact-full!)
             (bind-error (fn [data] [nil {}])) ; Clean Form, contact is stored
             (bind-error (partial success-message
                                  (str "Contact created successfully. "
                                       "Go back or create a new contact.")))
             (render-edit-contact! (:identity req)))))
```

As seen, the render function is going to be called anyway but with different
arguments depending if the validation and creation functions were executed
successfully or not.

For more info about this method, check [this post of Adam Bard's
blog](https://adambard.com/blog/acceptable-error-handling-in-clojure/).

## Future work

### Database queries

Database access should be improved if there's a need for complex queries.
At the moment, some of the database queries are mixed with clojure code for
simplicity and that may reduce the performance of the server and its
flexibility.

There are few options to improve that:

#### Improve HugSQL use with snippets and inline Clojure

HugSQL supports snippets and inline Clojure expressions as extensions. Those
can really improve the flexibility of the queries.

The main problem is HugSQL documentation is a bit hard to follow.

#### Move database access to HoneySQL

In order to have a more flexible database access, we propose to change the
query library to HoneySQL in the future. That's not the default database access
in Luminus so we asked the author of the framework to hear his opinion about
feasibility of the change, here is his answer:

https://mastodon.social/@yogthos/101177396622519004

> It should be straight forward to swap HugSQL out for it. You'd want to swap out
> conman for hikari-cp and add the honeysql dependency. Then, you'd initialize
> the connection via hikari, and use the standard clojure.java.jdbc function to
> work with it.
> -- Dimitri Sotnikov (Luminus framework author)

### Database architecture

The database has a quite monolitic architecture, but that was made in purpose
in order to reduce the amount of tables and simplify the queries.

That carries a risk.

The categories and the organizations of the contacts can be created by the
users with no control. The existing ones are just suggestions in the app. There
are different ways to achieve a similar result but most of them require a more
relational model which needs to handle many special cases for optional values
and so on.

At the moment, the app handles all those fields as optional and it suggests the
already used ones as possible values. Nothing stops the user in any sense. That
was also one of the goals of the app: to be as flexible as possible.

Of course, this can be a problem in the future if the amount of data grows
drastically. For that, the suggestion is to recreate the database architecture
to separate categories (and some other fields) as a different table and create
the necessary user interface to create and manage those fields.
